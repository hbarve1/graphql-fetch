const fetch = require("cross-fetch");

async function graphqlServer(URL, payload) {
  const options = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(payload),
  };

  const response = await fetch(URL, options);

  const json = await response.json();

  return json;
}

async function query(URL, { query, variables }) {
  const result = await graphqlServer(URL, { query, variables });

  return result;
}

async function mutation(URL, { mutation, variables }) {
  const result = await graphqlServer(URL, {
    query: mutation,
    variables,
  });

  return result;
}

module.exports = {
  query,
  mutation,
};
